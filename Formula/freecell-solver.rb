class FreecellSolver < Formula
  desc "Solitaire Solver library"
  homepage "https://fc-solve.shlomifish.org/"
  url "https://fc-solve.shlomifish.org/downloads/fc-solve/freecell-solver-6.0.1.tar.xz"
  # https://fc-solve.shlomifish.org/downloads/fc-solve/freecell-solver-5.24.0.tar.xz
  sha256 "9f1a4c6d5c8ac54c6619b3b988efb5562d460cd048d33345e52a0c849fd0d9df"
  revision 1

  depends_on "cmocka"

  def install
    # TODO: not too sure how we could automate these perl and python dependencies using homebrew?
    # ["Moo", "Path::Tiny", "Template", "Test::Data::Split"].each { |dep| system "cpan", dep }
    # pip3 install pysol_cards cffi freecell_solver

    mkdir "build" do
      system "cmake", "..", *std_cmake_args
      system "make"
      system "make", "install"
      prefix.install "install_manifest.txt"
    end
  end
end
